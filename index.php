<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="estilos.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menú de navegación</title>
</head>
<body>
<nav>
<ul>
    <?php
        $menu = array("Inicio", "Blog", "Videos", "Contacto");

        for ($i=0; $i < count($menu) ; $i++) { 
            echo '<li><a href="#">'.$menu[$i].'</a></li>';
        }

    ?>
    </ul>
    </nav>
</body>
</html>